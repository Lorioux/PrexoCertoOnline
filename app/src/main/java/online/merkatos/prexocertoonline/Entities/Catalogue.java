package online.merkatos.prexocertoonline.Entities;

import java.util.Date;

/**
 * SYNOPSIS
 * This is a entity representing a store catalogue for registered products
 */

class Catalogue {

    private String mCatalogOwner;
    private int mNumberOfProducts;
    private Date mCatalogCreateDate;
    private Date mCatalogModificationDate;
    private int ShopCode;


    public Catalogue() {
    }

}